package helpers;

public class CollisionInfo
{
	private boolean isGrounded;
	private int offset;

	/**
	 * 
	 * @param isGrounded true if the boundingRectangle should be able to jump
	 * @param offset the amount the boundingRectangle overshot, making it a collision.
	 */
	public CollisionInfo(boolean isGrounded, int offset)
	{
		this.isGrounded = isGrounded;
		this.offset = offset;
	}
	
	public boolean isGrounded()
	{
		return isGrounded;
	}

	public int getOffset()
	{
		return offset;
	}
}
