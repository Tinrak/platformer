package helpers;

public class Enums
{
	public enum ItemType
	{
		OneHand, OffHand, TwoHand, Helm, Shield, Chest, Legs, Feet, Error
	}

	public enum CollisionType
	{
		None, Blocked, BottomLeft, BottomRight, TopLeft, TopRight
	}

	public enum ElementType
	{
		Normal, Fire, Ice, Earth, Air
	}

}
