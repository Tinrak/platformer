package helpers;

public class SaveGame
{
	// public static class JsonWorld
	// {
	// public ArrayList<JsonArea> areaList = new ArrayList<JsonArea>();
	// public String currentArea;
	// public JsonPlayer player;
	// }
	//
	// public static class JsonPlayer
	// {
	// public int playerPosX;
	// public int playerPosY;
	// }
	//
	// public static class JsonArea
	// {
	// public String areaName;
	// public ArrayList<JsonEnemy> enemyList = new ArrayList<JsonEnemy>();
	// public ArrayList<JsonChest> chestList = new ArrayList<JsonChest>();
	// }
	//
	// public static class JsonEnemy
	// {
	// public int hp;
	// public int mp;
	// public boolean isAlive;
	// }
	//
	// public static class JsonChest
	// {
	// public boolean isOpen;
	// }
	//
	// public static void saveWorld(World world)
	// {
	// JsonWorld jWorld = new JsonWorld();
	// jWorld.currentArea = world.getCurrentArea().getAreaName();
	// JsonPlayer jPlayer = new JsonPlayer();
	// jPlayer.playerPosX = (int) world.getPlayer().getBoundingRectangle().x;
	// jPlayer.playerPosY = (int) world.getPlayer().getBoundingRectangle().y;
	// jWorld.player = jPlayer;
	// for (Area area : world.getAreas())
	// {
	// JsonArea jArea = new JsonArea();
	// jArea.areaName = area.getAreaName();
	//
	// for (Enemy enemy : area.getEnemies())
	// {
	// System.out.println("Enemy: " + enemy);
	// JsonEnemy jEnemy = new JsonEnemy();
	// jEnemy.hp = enemy.getHealthPoints();
	// jEnemy.mp = enemy.getManaPoints();
	// jEnemy.isAlive = enemy.isAlive();
	// jArea.enemyList.add(jEnemy);
	// }
	//
	// for (Chest chest : area.getChests())
	// {
	// System.out.println("Chest: " + chest);
	// JsonChest jChest = new JsonChest();
	// jChest.isOpen = chest.isOpened();
	// jArea.chestList.add(jChest);
	// }
	// jWorld.areaList.add(jArea);
	// }
	//
	// Json json = new Json();
	// writeFile("game.sav", json.toJson(jWorld));
	// }
	//
	// public static WorldData loadWorld()
	// {
	// String save = readFile("game.sav");
	// if (!save.isEmpty())
	// {
	// ArrayList<Area> areas = new ArrayList<Area>();
	// Json json = new Json();
	// JsonWorld jWorld = json.fromJson(JsonWorld.class, save);
	// WorldData worldData = new WorldData();
	//
	// if (jWorld.areaList != null)
	// {
	// for (JsonArea jArea : jWorld.areaList)
	// {
	// Area area = new Area(jArea.areaName);
	// if (jArea.areaName.equals(jWorld.currentArea))
	// {
	// System.out.println(jArea.areaName);
	// worldData.setCurrentArea(area);
	// }
	// if (jArea.enemyList != null)
	// {
	// for (int i = 0; i < jArea.enemyList.size(); i++)
	// {
	// Enemy newEnemy = area.getEnemies().get(i);
	// JsonEnemy oldEnemy = jArea.enemyList.get(i);
	// newEnemy.setHealthPoints(oldEnemy.hp);
	// newEnemy.setManaPoints(oldEnemy.mp);
	// newEnemy.setIsAlive(oldEnemy.isAlive);
	// }
	// }
	//
	// if (jArea.chestList != null)
	// {
	// for (int j = 0; j < jArea.chestList.size(); j++)
	// {
	// Chest newChest = area.getChests().get(j);
	// JsonChest oldChest = jArea.chestList.get(j);
	// newChest.setIsOpen(oldChest.isOpen);
	// }
	// }
	// areas.add(area);
	// }
	// worldData.setAreas(areas);
	//
	// worldData.setPlayerPosition(new Vector2(jWorld.player.playerPosX,
	// jWorld.player.playerPosY));
	// }
	// return worldData;
	// }
	// return null;
	// }
	//
	// public static void writeFile(String fileName, String s)
	// {
	// FileHandle file = Gdx.files.local(fileName);
	// // file.writeString(com.badlogic.gdx.utils.Base64Coder.encodeString(s),
	// // false);
	// file.writeString(s, false);
	// System.out.println("Game Saved!");
	// }
	//
	// public static String readFile(String fileName)
	// {
	// FileHandle file = Gdx.files.local(fileName);
	// if (file != null && file.exists())
	// {
	// String s = file.readString();
	// if (!s.isEmpty())
	// {
	// System.out.println("Game Loaded!");
	// return s;
	// // return com.badlogic.gdx.utils.Base64Coder.decodeString(s);
	// }
	// }
	//
	// return "";
	// }
}
