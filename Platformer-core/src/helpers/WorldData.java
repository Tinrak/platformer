package helpers;

import java.util.ArrayList;

import platformer.Area;

import com.badlogic.gdx.math.Vector2;

public class WorldData
{
	private ArrayList<Area> areas = new ArrayList<Area>();
	private Vector2 playerPosition = new Vector2(0, 0);
	private Area currentArea = null;

	public ArrayList<Area> getAreas()
	{
		return areas;
	}

	public void setAreas(ArrayList<Area> areas)
	{
		this.areas = areas;
	}

	public Vector2 getPlayerPosition()
	{
		return playerPosition;
	}

	public void setPlayerPosition(Vector2 playerPosition)
	{
		this.playerPosition = playerPosition;
	}

	public Area getCurrentArea()
	{
		return currentArea;
	}

	public void setCurrentArea(Area currentArea)
	{
		this.currentArea = currentArea;
	}
}
