package entities;

import helpers.CollisionInfo;
import platformer.Platformer;
import singletons.CollisionHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Player
{
	private CollisionHandler collisionHandler;
	private Sprite sprite;
	private Rectangle boundingRectangle;
	private Vector2 worldSize;

	private int velocityX, velocityY;
	private int moveSpeed = 200;
	private int jumpSpeed = 1500;
	private int gravity = -60;
	private boolean isGrounded = false;

	private int coinsAmount = 0;

	public Player(String playerName)
	{
		this.sprite = new Sprite(new Texture(Gdx.files.internal("Textures/" + playerName + ".png")));
		collisionHandler = CollisionHandler.getCollisionHandler();
		boundingRectangle = new Rectangle(0, 0, 32, 64);
	}

	public void changeLevel(Vector2 spawnPosition)
	{
		velocityX = 0;
		velocityY = 0;	
		boundingRectangle.x = spawnPosition.x;
		boundingRectangle.y = spawnPosition.y;
		worldSize = collisionHandler.getWorldSize();	
	}

	public void update(float delta)
	{
		updateHorizontalMovement(delta);
		updateVerticalMovement(delta);
		updatePlayer();
	}

	private void updateHorizontalMovement(float delta)
	{
		if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.D))
		{
			if(Gdx.input.isKeyPressed(Input.Keys.A) && Gdx.input.isKeyPressed(Input.Keys.D))
			{
				
			}
			else if(Gdx.input.isKeyPressed(Input.Keys.A))
			{
				velocityX -= (int) (moveSpeed * delta);
			}
			else if (Gdx.input.isKeyPressed(Input.Keys.D))
			{
				velocityX += (int) (moveSpeed * delta);
			}
		}
		else
		{
			velocityX /= 100*delta;
		}

		
		velocityX = (int) clampFloat(velocityX, -5, 5);

		int possibleMovementX = collisionHandler.checkCollisionHorizontal(boundingRectangle, velocityX);
		boundingRectangle.x += possibleMovementX;
		boundingRectangle.x = clampFloat(boundingRectangle.x, 0, worldSize.x);
	}

	private void updateVerticalMovement(float delta)
	{
		velocityY += gravity * delta;
		if (velocityY < -20)
		{
			velocityY = -20;
		}

		if (isGrounded && Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
		{
			isGrounded = false;
			velocityY = (int) (jumpSpeed * delta);
		}

		int possibleMovementY = 0;
		if (velocityY < 0) // Check collision down and handle slopes
		{
			isGrounded = false; // We are falling => can�t jump.
			CollisionInfo ci = collisionHandler.checkCollisonDown(boundingRectangle, velocityY);
			possibleMovementY = ci.getOffset();
			
			// If a upward slope occurred possibleMovementY is larger than 0 =>
			// check for collision up.
			if (possibleMovementY > 0)
			{
				possibleMovementY = collisionHandler.checkCollisionUp(boundingRectangle, possibleMovementY);
			}
			isGrounded = ci.isGrounded();
		} else if (velocityY > 0) // We are jumping => check for collision up.
		{
			possibleMovementY = collisionHandler.checkCollisionUp(boundingRectangle, velocityY);
			if (possibleMovementY == 0)
			{
				velocityY = 0;
			}
		}

		boundingRectangle.y += possibleMovementY;
		boundingRectangle.y = clampFloat(boundingRectangle.y, 0, worldSize.y);
	}

	/**
	 * Updates the position of the sprite and the camera.
	 */
	private void updatePlayer()
	{
		sprite.setPosition(boundingRectangle.x, boundingRectangle.y);
		Platformer.camera.position.x = boundingRectangle.x + boundingRectangle.width / 2;
		Platformer.camera.position.y = boundingRectangle.y + boundingRectangle.height / 2;
	}

	private float clampFloat(float value, float min, float max)
	{
		return Math.max(min, Math.min(max, value));
	}

	public Sprite getSprite()
	{
		return sprite;
	}

	public int getCoins()
	{
		return coinsAmount;
	}

	public void addCoins(int amount)
	{
		coinsAmount += amount;
	}

	public Rectangle getBoundingRectangle()
	{
		return boundingRectangle;
	}
}
