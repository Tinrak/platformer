package entities;

import helpers.Enums.ItemType;

public class Item
{
	protected String itemName;
	protected int itemID;
	protected int value;
	protected String itemDescription;
	protected ItemType type;

	public Item(String itemName, int itemID, int value, String itemDescription, ItemType type)
	{
		this.itemName = itemName;
		this.itemID = itemID;
		this.value = value;
		this.itemDescription = itemDescription;
		this.type = type;
	}

	public String getItemName()
	{
		return itemName;
	}

	public int getItemID()
	{
		return itemID;
	}

	public int getValue()
	{
		return value;
	}

	public String getItemDescription()
	{
		return itemDescription;
	}

	public ItemType getType()
	{
		return type;
	}
}
