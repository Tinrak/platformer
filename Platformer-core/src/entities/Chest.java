package entities;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Chest
{
	private ArrayList<Item> content = new ArrayList<Item>();
	private Rectangle boundingRectangle;
	private int coins = 0;
	private Sprite sprite;
	private boolean isOpen;

	public Chest(int coinsAmount, Rectangle boundingRectangle)
	{
		this.coins = coinsAmount;
		this.boundingRectangle = boundingRectangle;
		isOpen = false;

		sprite = new Sprite(new Texture(Gdx.files.internal("Textures/ChestClosed.png")));
		sprite.setX(boundingRectangle.x);
		sprite.setY(boundingRectangle.y);
	}

	public void addItem(Item item)
	{
		content.add(item);
	}

	public void setIsOpen(boolean isOpen)
	{
		this.isOpen = isOpen;
		sprite = new Sprite(new Texture(Gdx.files.internal("Textures/ChestOpen.png")));
		sprite.setX(boundingRectangle.x);
		sprite.setY(boundingRectangle.y);
	}

	public ArrayList<Item> getContent()
	{
		return content;
	}

	public int getCoins()
	{
		return coins;
	}

	public Sprite getSprite()
	{
		return sprite;
	}

	public Rectangle getBoundingRectangle()
	{
		return boundingRectangle;
	}

	public boolean isOpened()
	{
		return isOpen;
	}
}
