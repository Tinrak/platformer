package entities;

import helpers.Enums.ElementType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Enemy
{
	private String enemyName;
	private Rectangle boundingRectangle;
	private int healthPoints, maxHealthPoints;
	private int manaPoints, maxManaPoints;
	private int strength, intelligence, agility;
	private ElementType elementWeakness, elementStrength;
	private int xpGained;
	private boolean isAlive;

	private Sprite[][] spriteSheet;
	private Sprite currentSprite;
	private float stateTime;
	private int currentRow = 0;
	private int currentColumn = 0;
	private int maxRowLenght = 0;
	private int maxColumnLenght = 0;

	// Constructors
	public Enemy(String name, int maxHealthPoints, int maxManaPoints, int strength, int intelligence, int agility,
			ElementType elementWeakness, ElementType elementStrength, int xpGained)
	{
		this.enemyName = name;
		this.healthPoints = maxHealthPoints;
		this.maxHealthPoints = maxHealthPoints;
		this.manaPoints = maxManaPoints;
		this.maxManaPoints = maxManaPoints;
		this.strength = strength;
		this.intelligence = intelligence;
		this.agility = agility;
		this.elementWeakness = elementWeakness;
		this.elementStrength = elementStrength;
		this.xpGained = xpGained;
		this.isAlive = true;
	}

	public Enemy(Enemy enemy, Rectangle boundingRectangle, Vector2 textureSize)
	{
		this(enemy.getEnemyName(), enemy.getMaxHealthPoints(), enemy.getMaxManaPoints(), enemy.getStrength(), enemy
				.getIntelligence(), enemy.getAgility(), enemy.getElementWeakness(), enemy.getElementStrength(), enemy
				.getXpGained());

		this.boundingRectangle = boundingRectangle;

		Texture texture = new Texture(Gdx.files.internal("Textures/" + enemyName + ".png"));

		TextureRegion[][] tmp = TextureRegion.split(texture, (int) textureSize.x, (int) textureSize.y);
		maxColumnLenght = tmp[0].length;
		maxRowLenght = tmp.length;

		spriteSheet = new Sprite[maxRowLenght][maxColumnLenght];

		for (int i = 0; i < maxRowLenght; i++)
		{
			for (int j = 0; j < maxColumnLenght; j++)
			{
				spriteSheet[i][j] = new Sprite(tmp[i][j]);
			}
		}

		this.currentSprite = spriteSheet[currentRow][currentColumn];
		stateTime = 0.0f;
		currentSprite.setX(boundingRectangle.x);
		currentSprite.setY(boundingRectangle.y);
	}

	public void update(float delta)
	{
		if (!isAlive)
		{
			return;
		}
		stateTime += delta;
		if (stateTime > 0.15f)
		{
			currentSprite = spriteSheet[currentRow][clampColumn()];
			stateTime = 0;
		}
		currentSprite.setX(boundingRectangle.x);
		currentSprite.setY(boundingRectangle.y);
	}

	private int clampColumn()
	{
		currentColumn++;
		if (currentColumn >= maxColumnLenght)
		{
			currentColumn = 0;
		}
		return currentColumn;
	}

	// Setters and Getters
	public String getEnemyName()
	{
		return enemyName;
	}

	public Rectangle getBoundingRectangle()
	{
		return boundingRectangle;
	}

	public int getHealthPoints()
	{
		return healthPoints;
	}

	public void setHealthPoints(int healthPoints)
	{
		this.healthPoints = healthPoints;
	}

	public int getMaxHealthPoints()
	{
		return maxHealthPoints;
	}

	public void setMaxHealthPoints(int maxHealthPoints)
	{
		this.maxHealthPoints = maxHealthPoints;
	}

	public int getManaPoints()
	{
		return manaPoints;
	}

	public void setManaPoints(int manaPoints)
	{
		this.manaPoints = manaPoints;
	}

	public int getMaxManaPoints()
	{
		return maxManaPoints;
	}

	public void setMaxManaPoints(int maxManaPoints)
	{
		this.maxManaPoints = maxManaPoints;
	}

	public int getStrength()
	{
		return strength;
	}

	public int getIntelligence()
	{
		return intelligence;
	}

	public int getAgility()
	{
		return agility;
	}

	public ElementType getElementWeakness()
	{
		return elementWeakness;
	}

	public ElementType getElementStrength()
	{
		return elementStrength;
	}

	public int getXpGained()
	{
		return xpGained;
	}

	public Sprite getSprite()
	{
		return currentSprite;
	}

	public boolean isAlive()
	{
		return isAlive;
	}

	public void setIsAlive(boolean isAlive)
	{
		this.isAlive = isAlive;
	}
}
