package entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Door
{
	private int doorID;
	private String destination;
	private int destinationDoorID;
	private Rectangle boundingRectangle;
	private boolean facingRight;
	private Sprite sprite;
	

	public Door(Rectangle boundingRectangle, String destination, int destinationDoorID, int doorID, boolean facingRight)
	{
		this.doorID = doorID;
		this.destination = destination;
		this.destinationDoorID = destinationDoorID;
		this.boundingRectangle = boundingRectangle;
		this.facingRight = facingRight;
		sprite = new Sprite(new Texture(Gdx.files.internal("Textures/Door.png")));
		if(facingRight)
		{
			sprite.setX(boundingRectangle.x);
			sprite.setY(boundingRectangle.y);
		}
		else
		{
			sprite.flip(true, false);
			sprite.setX(boundingRectangle.x - boundingRectangle.width);
			sprite.setY(boundingRectangle.y);
		}
	}

	public int getDoorID()
	{
		return doorID;
	}
	
	public String getDestination()
	{
		return destination;
	}

	public int getDestinationDoorID()
	{
		return destinationDoorID;
	}

	public Rectangle getBoundingRectangle()
	{
		return boundingRectangle;
	}
	
	public Sprite getSprite()
	{
		return sprite;
	}
	
	public boolean isFacingRight()
	{
		return facingRight;
	}


}
