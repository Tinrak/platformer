package singletons;

import helpers.Enums.ElementType;
import helpers.Enums.ItemType;

import java.io.IOException;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import entities.Enemy;
import entities.Item;

public class WorldCollection
{
	private ArrayList<Item> itemCollection = new ArrayList<Item>();
	private ArrayList<Enemy> enemyCollection = new ArrayList<Enemy>();
	private XmlReader reader = new XmlReader();
	private String itemCollectionPath = "GameContent/Items.xml";
	private String EnemyCollectionPath = "GameContent/Enemies.xml";

	private static WorldCollection _worldCollection;

	private WorldCollection()
	{
		fillItemCollection();
		fillEnemyCollection();
	}

	public static WorldCollection getWorldCollection()
	{
		if (_worldCollection == null)
		{
			_worldCollection = new WorldCollection();
		}
		return _worldCollection;
	}

	private void fillItemCollection()
	{
		Array<Element> items = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(itemCollectionPath));
			items = root.getChildrenByName("item");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + itemCollectionPath);
		}

		for (Element child : items)
		{
			String itemName = child.get("name");
			int itemID = child.getInt("id");
			int itemValue = child.getChildByName("value").getIntAttribute("amount");
			String itemDescription = child.getChildByName("description").getAttribute("text");
			ItemType itemType = stringToType(child.getChildByName("type").getAttribute("type"));

			Item item = new Item(itemName, itemID, itemValue, itemDescription, itemType);

			itemCollection.add(item);
		}
	}

	private void fillEnemyCollection()
	{
		Array<Element> enemies = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(EnemyCollectionPath));
			enemies = root.getChildrenByName("enemy");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + EnemyCollectionPath);
		}

		for (Element child : enemies)
		{
			String enemyName = child.get("name");
			int maxHP = child.getChildByName("stats").getIntAttribute("hp");
			int maxMP = child.getChildByName("stats").getIntAttribute("mp");
			int str = child.getChildByName("stats").getIntAttribute("str");
			int _int = child.getChildByName("stats").getIntAttribute("int");
			int agi = child.getChildByName("stats").getIntAttribute("agi");
			ElementType weakness = stringToElement(child.getChildByName("element").getAttribute("weakness"));
			ElementType strength = stringToElement(child.getChildByName("element").getAttribute("strength"));
			int xpGained = child.getChildByName("experience").getIntAttribute("gained");

			Enemy enemy = new Enemy(enemyName, maxHP, maxMP, str, _int, agi, weakness, strength, xpGained);
			enemyCollection.add(enemy);
		}
	}

	private ItemType stringToType(String input)
	{
		switch (input)
		{
			case "1HAND":
				return ItemType.OneHand;
			case "2HAND":
				return ItemType.TwoHand;
			default:
				return ItemType.Error;
		}
	}

	private ElementType stringToElement(String input)
	{
		switch (input)
		{
			case "FIRE":
				return ElementType.Fire;
			case "ICE":
				return ElementType.Ice;
			case "EARTH":
				return ElementType.Earth;
			case "AIR":
				return ElementType.Air;
			default:
				return ElementType.Normal;
		}
	}

	public ArrayList<Item> getItemCollection()
	{
		return itemCollection;
	}

	public ArrayList<Enemy> getEnemyCollection()
	{
		return enemyCollection;
	}
}
