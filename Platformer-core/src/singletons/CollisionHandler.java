package singletons;

import helpers.CollisionInfo;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class CollisionHandler
{
	private static CollisionHandler _collisionHandler;

	private TiledMapTileLayer collisionLayer;
	private Vector2 worldSize = new Vector2(0, 0);
	private Rectangle tileRectangle = new Rectangle(0, 0, 0, 0);

	private CollisionHandler()
	{
	}

	public static CollisionHandler getCollisionHandler()
	{
		if (_collisionHandler == null)
		{
			_collisionHandler = new CollisionHandler();
		}
		return _collisionHandler;
	}

	public void changeMap(TiledMapTileLayer collisionLayer)
	{
		this.collisionLayer = collisionLayer;
		collisionLayer.setVisible(false);
		tileRectangle.width = collisionLayer.getTileWidth();
		tileRectangle.height = collisionLayer.getTileHeight();
		worldSize.x = tileRectangle.width * (collisionLayer.getWidth() - 1);
		worldSize.y = tileRectangle.height * (collisionLayer.getHeight() - 1);
	}

	public int checkCollisionHorizontal(Rectangle boundingRectangle, int velocityX)
	{
		if (velocityX > 0) // Right
		{
			int top = getMapBoundsY((boundingRectangle.y + boundingRectangle.height) / tileRectangle.height);
			int middle = getMapBoundsY((boundingRectangle.y + (boundingRectangle.height / 2)) / tileRectangle.height);
			int bottom = getMapBoundsY((boundingRectangle.y) / tileRectangle.height);
			int posX = getMapBoundsX((boundingRectangle.x + boundingRectangle.width) / tileRectangle.height);
			// System.out.println("PosX: " + posX + " | YB: " + bottom +
			// " | YM: " + middle + " | YT: " + top);

			for (int i = posX; i < collisionLayer.getWidth(); i++)
			{
				if ((i * tileRectangle.width) > (boundingRectangle.x + boundingRectangle.width + velocityX))
				{
					return velocityX;
				}

				TiledMapTile tile = collisionLayer.getCell(i, top).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					if (!tile.getProperties().get("Collision").equals("SlopeBottomRight"))
					{
						return (int) (velocityX - ((boundingRectangle.x + boundingRectangle.width + velocityX) - (i * tileRectangle.width))) - 1;
					}
				}
				tile = collisionLayer.getCell(i, middle).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					return (int) (velocityX - ((boundingRectangle.x + boundingRectangle.width + velocityX) - (i * tileRectangle.width))) - 1;
				}
				tile = collisionLayer.getCell(i, bottom).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					if (!tile.getProperties().get("Collision").equals("SlopeBottomRight"))
					{
						return (int) (velocityX - ((boundingRectangle.x + boundingRectangle.width + velocityX) - (i * tileRectangle.width))) - 1;
					}
				}
			}

		} else if (velocityX < 0) // Left
		{
			int top = getMapBoundsY((boundingRectangle.y + boundingRectangle.height) / tileRectangle.height);
			int middle = getMapBoundsY((boundingRectangle.y + (boundingRectangle.height / 2)) / tileRectangle.height);
			int bottom = getMapBoundsY((boundingRectangle.y) / tileRectangle.height);
			int posX = getMapBoundsX((boundingRectangle.x) / tileRectangle.height);
			// System.out.println("PosX: " + posX + " | YB: " + bottom +
			// " | YM: " + middle + " | YT: " + top);

			for (int i = posX; i >= 0; i--)
			{
				if ((i * tileRectangle.width) + tileRectangle.width < boundingRectangle.x + velocityX)
				{
					return velocityX;
				}

				TiledMapTile tile = collisionLayer.getCell(i, top).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					if (!tile.getProperties().get("Collision").equals("SlopeBottomLeft"))
					{
						return (int) (velocityX + (((i * tileRectangle.width) + tileRectangle.width) - (boundingRectangle.x + velocityX))) + 1;
					}
				}
				tile = collisionLayer.getCell(i, middle).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					return (int) (velocityX + (((i * tileRectangle.width) + tileRectangle.width) - (boundingRectangle.x + velocityX))) + 1;

				}
				tile = collisionLayer.getCell(i, bottom).getTile();
				if (tile.getProperties().containsKey("Collision"))
				{
					if (!tile.getProperties().get("Collision").equals("SlopeBottomLeft"))
					{
						return (int) (velocityX + (((i * tileRectangle.width) + tileRectangle.width) - (boundingRectangle.x + velocityX))) + 1;
					}
				}
			}
		}

		return velocityX;
	}

	public CollisionInfo checkCollisonDown(Rectangle boundingRectangle, int velocityY)
	{
		int left = getMapBoundsX((boundingRectangle.x) / tileRectangle.width);
		int right = getMapBoundsX((boundingRectangle.x + boundingRectangle.width) / tileRectangle.width);
		int posY = getMapBoundsY((boundingRectangle.y) / tileRectangle.height);

		for (int i = posY; i >= 0; i--)
		{
			if ((i * tileRectangle.height) + tileRectangle.height < boundingRectangle.y + velocityY)
			{
				return new CollisionInfo(false, velocityY);
			}

			TiledMapTile tile = collisionLayer.getCell(left, i).getTile();
			if (tile.getProperties().containsKey("Collision"))
			{
				if (!tile.getProperties().get("Collision").equals("SlopeBottomLeft"))
				{
					return new CollisionInfo(
							true,
							(int) (velocityY + (((i * tileRectangle.height) + tileRectangle.height) - (boundingRectangle.y + velocityY))));
				} else
				{
					int offset = (int) (velocityY + (((i * tileRectangle.height) + tileRectangle.height) - (boundingRectangle.y + velocityY)));
					offset -= (boundingRectangle.x % tileRectangle.width);
					return new CollisionInfo(true, offset + 1);
				}
			}
			tile = collisionLayer.getCell(right, i).getTile();
			if (tile.getProperties().containsKey("Collision"))
			{
				if (!tile.getProperties().get("Collision").equals("SlopeBottomRight"))
				{
					return new CollisionInfo(
							true,
							(int) (velocityY + (((i * tileRectangle.height) + tileRectangle.height) - (boundingRectangle.y + velocityY))));
				} else
				{
					int offset = (int) (velocityY + (((i * tileRectangle.height) + tileRectangle.height) - (boundingRectangle.y + velocityY)));
					offset -= tileRectangle.width - (boundingRectangle.x % tileRectangle.width);
					return new CollisionInfo(true, offset + 1);
				}
			}
		}
		return new CollisionInfo(false, velocityY);
	}

	public int checkCollisionUp(Rectangle boundingRectangle, int velocityY)
	{
		int left = getMapBoundsX((boundingRectangle.x) / tileRectangle.width);
		int right = getMapBoundsX((boundingRectangle.x + boundingRectangle.width) / tileRectangle.width);
		int posY = getMapBoundsY((boundingRectangle.y + boundingRectangle.height) / tileRectangle.height);

		for (int i = posY; i < collisionLayer.getHeight(); i++)
		{
			if ((i * tileRectangle.width) > (boundingRectangle.y + boundingRectangle.height + velocityY))
			{
				return velocityY;
			}

			TiledMapTile tile = collisionLayer.getCell(left, i).getTile();
			if (tile.getProperties().containsKey("Collision"))
			{
				return (int) (velocityY - ((boundingRectangle.y + boundingRectangle.height + velocityY) - (i * tileRectangle.height))) - 1;
			}
			tile = collisionLayer.getCell(right, i).getTile();
			if (tile.getProperties().containsKey("Collision"))
			{
				return (int) (velocityY - ((boundingRectangle.y + boundingRectangle.height + velocityY) - (i * tileRectangle.height))) - 1;
			}
		}
		return velocityY;
	}

	private int getMapBoundsX(float number)
	{
		return (int) Math.min(Math.max(0, number), collisionLayer.getWidth() - 1);
	}

	private int getMapBoundsY(float number)
	{
		return (int) Math.min(Math.max(0, number), collisionLayer.getHeight() - 1);
	}

	public TiledMapTileLayer getCollisionLayer()
	{
		return collisionLayer;
	}

	public Vector2 getWorldSize()
	{
		return worldSize;
	}

}
