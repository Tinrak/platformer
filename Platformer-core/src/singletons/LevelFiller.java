package singletons;

import java.io.IOException;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import entities.Chest;
import entities.Door;
import entities.Enemy;
import entities.Item;

public class LevelFiller
{
	private static LevelFiller _levelFiller;

	private XmlReader reader = new XmlReader();
	private ArrayList<Item> itemCollection;
	private ArrayList<Enemy> enemyCollection;
	private WorldCollection worldCollection;

	private LevelFiller()
	{
		worldCollection = WorldCollection.getWorldCollection();
		itemCollection = worldCollection.getItemCollection();
		enemyCollection = worldCollection.getEnemyCollection();
	}

	public static LevelFiller getLevelFiller()
	{
		if (_levelFiller == null)
		{
			_levelFiller = new LevelFiller();
		}
		return _levelFiller;
	}

	public ArrayList<Chest> createChests(String levelName, TiledMap tiledMap)
	{
		ArrayList<Chest> chests = new ArrayList<Chest>();
		String path = "Levels/" + levelName + "/Chests.xml";
		Array<Element> chestsElements = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(path));
			chestsElements = root.getChildrenByName("chest");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + path);
		}
		
		for(MapObject object : tiledMap.getLayers().get("Chests").getObjects())
		{
			if(object instanceof RectangleMapObject)
			{
				Rectangle boundingRectangle = ((RectangleMapObject) object).getRectangle();
				for (Element child : chestsElements)
				{
					if(object.getName().equals(child.get("ID")))
					{
						Chest chest = new Chest(child.getChildByName("coins").getIntAttribute("amount"), boundingRectangle);
						Array<Element> chestContent = child.getChildrenByName("content");
						for (Element itemChild : chestContent)
						{
							chest.addItem(itemCollection.get(itemChild.getIntAttribute("itemID")));
						}
						chests.add(chest);
					}
				}
			}
		}
		return chests;
	}

	public ArrayList<Enemy> createEnemies(String levelName)
	{
		ArrayList<Enemy> enemies = new ArrayList<Enemy>();
		String path = "Levels/" + levelName + "/Enemies.xml";
		Array<Element> enemyElements = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(path));
			enemyElements = root.getChildrenByName("enemy");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + path);
		}

		for (Element child : enemyElements)
		{
			float posX = child.getFloat("posX") * 32;
			float posY = child.getFloat("posY") * 32;
			float width = child.getFloat("width");
			float height = child.getFloat("height");
			Rectangle boundingRectangle = new Rectangle(posX, posY, width, height);
			enemies.add(getNewEnemy(child.getChildByName("enemyID").getIntAttribute("ID"), boundingRectangle,
					new Vector2(width, height)));
		}
		return enemies;
	}

	public ArrayList<Door> createDoors(String levelName, TiledMap tiledMap)
	{
		ArrayList<Door> doors = new ArrayList<Door>();
		String path = "Levels/" + levelName + "/LevelInfo.xml";
		Array<Element> doorElements = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(path));
			doorElements = root.getChildrenByName("door");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + path);
		}

		for(MapObject object : tiledMap.getLayers().get("Doors").getObjects())
		{
			if(object instanceof RectangleMapObject)
			{
				Rectangle boundingRectangle = ((RectangleMapObject) object).getRectangle();
				for (Element child : doorElements)
				{
					if(object.getName().equals(child.get("ID")))
					{
						String leadsToRoom = child.getChildByName("doorDestination").get("name");
						int leadsToDoorID = child.getChildByName("doorDestinationID").getIntAttribute("destinationID");
						int doorID = child.getIntAttribute("ID");
						
						boolean facingRight = false;
						if(object.getProperties().get("Direction").equals("Right"))
						{
							facingRight = true;
						}

						doors.add(new Door(boundingRectangle, leadsToRoom, leadsToDoorID, doorID, facingRight));
					}
				}
			}
		}
		return doors;
	}
	
	public Vector2 getSpawnPoint(String levelName)
	{
		String path = "Levels/" + levelName + "/LevelInfo.xml";
		Array<Element> doorElements = new Array<Element>();
		Element root;

		try
		{
			root = reader.parse(Gdx.files.internal(path));
			doorElements = root.getChildrenByName("spawnPoint");
		} catch (IOException e)
		{
			System.out.println("Error loading XML file!: " + path);
		}
		
		if(doorElements.size == 0)
		{
			
		}
		else
		{
			
		}
		
		return null;
	}

	private Enemy getNewEnemy(int index, Rectangle boundingRectangle, Vector2 textureSize)
	{
		return new Enemy(enemyCollection.get(index), boundingRectangle, textureSize);
	}
}
