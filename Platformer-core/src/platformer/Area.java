package platformer;

import java.util.ArrayList;

import singletons.CollisionHandler;
import singletons.LevelFiller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

import entities.Chest;
import entities.Door;
import entities.Enemy;
import entities.Item;

public class Area
{
	private ArrayList<Chest> chests = new ArrayList<Chest>();
	private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	private ArrayList<Door> doors = new ArrayList<Door>();
	private LevelFiller levelFiller;
	private String areaName;
	private TiledMap tiledMap;
	private TiledMapRenderer tiledMapRenderer;
	private CollisionHandler collisionHandler;
	private World world;
	private Vector2 spawnPoint;
	public boolean areaLoaded = false;

	public Area(String areaName)
	{
		this.areaName = areaName;
		this.collisionHandler = CollisionHandler.getCollisionHandler();
		this.levelFiller = LevelFiller.getLevelFiller();
	}

	public void loadLevel(World world)
	{
		this.world = world;
		tiledMap = new TmxMapLoader().load("Levels/" + areaName + "/Map.tmx");
		tiledMapRenderer = new TiledMapRenderer(tiledMap);
		collisionHandler.changeMap((TiledMapTileLayer) tiledMap.getLayers().get("Collision"));
		
		enemies = levelFiller.createEnemies(areaName);
		chests = levelFiller.createChests(areaName, tiledMap);
		doors = levelFiller.createDoors(areaName, tiledMap);
		spawnPoint = levelFiller.getSpawnPoint(areaName);
		
		tiledMapRenderer.addEnemies(enemies);
		tiledMapRenderer.addChests(chests);
		tiledMapRenderer.addDoors(doors);
		tiledMapRenderer.addPlayer(world.getPlayer());
		
		areaLoaded = true;
	}

	public void update(float delta)
	{
		for (Enemy enemy : enemies)
		{
			enemy.update(delta);
			if (Intersector.overlaps(world.getPlayer().getBoundingRectangle(), enemy.getBoundingRectangle()))
			{
				enemy.setIsAlive(false);
			}
		}

		for (Chest chest : chests)
		{
			if (!chest.isOpened())
			{
				if (Intersector.overlaps(world.getPlayer().getBoundingRectangle(), chest.getBoundingRectangle()))
				{
					chest.setIsOpen(true);
					for(Item item : chest.getContent())
					{
						System.out.println(item.getItemName());
					}
				}
			}
		}

		for (Door door : doors)
		{
			if (Intersector.overlaps(world.getPlayer().getBoundingRectangle(), door.getBoundingRectangle()))
			{
				world.switchToLevel(door.getDestination(), door.getDestinationDoorID());
			}
		}

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Platformer.camera.update();
		tiledMapRenderer.setView(Platformer.camera);
		tiledMapRenderer.render();
	}

	public ArrayList<Enemy> getEnemies()
	{
		return enemies;
	}

	public ArrayList<Chest> getChests()
	{
		return chests;
	}

	public ArrayList<Door> getDoors()
	{
		return doors;
	}

	public String getAreaName()
	{
		return areaName;
	}

	public Vector2 getSpawnPoint()
	{
		return spawnPoint;
	}
}
