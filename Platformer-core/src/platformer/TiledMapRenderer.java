package platformer;

import java.util.ArrayList;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import entities.Chest;
import entities.Door;
import entities.Enemy;
import entities.Player;

public class TiledMapRenderer extends OrthogonalTiledMapRenderer
{
	private ArrayList<Player> playerSprites;
	private ArrayList<Enemy> enemySprites;
	private ArrayList<Chest> chestSprites;
	private ArrayList<Door> doorSprites;
	private int drawSpritesAfterLayer = 1;

	// Ground -> BackGround -> Entities -> ForeGround

	public TiledMapRenderer(TiledMap map)
	{
		super(map);
		playerSprites = new ArrayList<Player>();
		enemySprites = new ArrayList<Enemy>();
		chestSprites = new ArrayList<Chest>();
		doorSprites = new ArrayList<Door>();
	}

	public void addPlayer(Player player)
	{
		playerSprites.add(player);
	}

	public void addEnemy(Enemy enemy)
	{
		enemySprites.add(enemy);
	}

	public void addChest(Chest chest)
	{
		chestSprites.add(chest);
	}
	
	public void addDoor(Door door)
	{
		doorSprites.add(door);
	}

	public void addPlayers(ArrayList<Player> players)
	{
		playerSprites.addAll(players);
	}

	public void addEnemies(ArrayList<Enemy> enemies)
	{
		enemySprites.addAll(enemies);
	}

	public void addChests(ArrayList<Chest> chests)
	{
		chestSprites.addAll(chests);
	}
	
	public void addDoors(ArrayList<Door> doors)
	{
		doorSprites.addAll(doors);
	}

	@Override
	public void render()
	{
		beginRender();
		int currentLayer = 0;
		for (MapLayer layer : map.getLayers())
		{
			if (layer.isVisible())
			{
				if (layer instanceof TiledMapTileLayer)
				{
					renderTileLayer((TiledMapTileLayer) layer);
					currentLayer++;
					if (currentLayer == drawSpritesAfterLayer)
					{
						for (Chest chest : chestSprites)
						{
							chest.getSprite().draw(this.getSpriteBatch());
						}
						for (Enemy enemy : enemySprites)
						{
							enemy.getSprite().draw(this.getSpriteBatch());
						}
						for (Player player : playerSprites)
						{
							player.getSprite().draw(this.getSpriteBatch());
						}
						for (Door door : doorSprites)
						{
							door.getSprite().draw(this.getSpriteBatch());
						}
					}
				} else
				{
					for (MapObject object : layer.getObjects())
					{
						renderObject(object);
					}
				}
			}
		}
		endRender();
	}
}
