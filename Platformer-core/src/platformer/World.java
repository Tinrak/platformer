package platformer;

import singletons.CollisionHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

import entities.Door;
import entities.Player;

public class World
{
	private Area currentArea;
	private Player currentPlayer;

	protected World()
	{
		currentPlayer = new Player("Player");
		switchToLevel("Level1", 1);
	}

	public void update(float delta)
	{
		if (Platformer.DEBUGGING)
		{
			if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_1))
			{
				CollisionHandler.getCollisionHandler().getCollisionLayer().setVisible(!CollisionHandler.getCollisionHandler().getCollisionLayer().isVisible());
			}
		}

		// Update area
		currentPlayer.update(delta);
		currentArea.update(delta);
	}

	protected void switchToLevel(String level, int destinationID)
	{
		System.out.println("Switched Level!");
		currentArea = new Area(level);
		currentArea.loadLevel(this);
		if(destinationID == -1)
		{
			if(currentArea.getSpawnPoint() == null)
			{
				System.out.println("Spawnpoint invalid!!");
				currentPlayer.changeLevel(new Vector2(600, 600));
			}
			else
			{
				//TODO wrong, this should NOT happen. If we loaded/started a game we should have a spawnpoint.
				// and if we came from another room the destinationID shouldn�t be -1!
				System.out.println("Player resetting to 640, 640. Change this!" );
				currentPlayer.changeLevel(new Vector2(600, 600));
			}
		}
		else
		{
			for(Door door : currentArea.getDoors())
			{
				if(door.getDoorID() == destinationID)
				{
					if(door.isFacingRight())
					{
						currentPlayer.changeLevel(new Vector2(door.getBoundingRectangle().x + 40, door.getBoundingRectangle().y));
					}
					else
					{
						currentPlayer.changeLevel(new Vector2(door.getBoundingRectangle().x - 40, door.getBoundingRectangle().y));
					}
				}
			}
		}
	}

	public Player getPlayer()
	{
		return currentPlayer;
	}

	public Area getCurrentArea()
	{
		return currentArea;
	}
}
