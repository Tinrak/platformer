package platformer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class MainMenu implements Screen
{
	private final Platformer game;
	private float screenHeight;

	protected MainMenu(final Platformer game)
	{
		this.game = game;
		screenHeight = Gdx.graphics.getHeight();
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		game.getHudSpriteBatch().begin();
		game.getFont().draw(game.getHudSpriteBatch(), "Platformer! ", 5, screenHeight - 5);
		game.getFont().draw(game.getHudSpriteBatch(), "Click to start a game!", 5, screenHeight - 25);
		game.getHudSpriteBatch().end();

		if (Gdx.input.isButtonPressed(0))
		{
			game.setScreen(game.getGameScreen());
			dispose();
		} else if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
		{
			Gdx.app.exit();
		}
	}

	@Override
	public void resize(int width, int height)
	{
		System.out.println("Main Menu Resize");
	}

	@Override
	public void show()
	{
		System.out.println("Main Menu Show");
	}

	@Override
	public void hide()
	{
		System.out.println("Main Menu Hide");
		dispose();
	}

	@Override
	public void pause()
	{
		System.out.println("Main Menu Pause");
	}

	@Override
	public void resume()
	{
		System.out.println("Main Menu Resume");
	}

	@Override
	public void dispose()
	{
		System.out.println("Main Menu Dispose");
	}
}
