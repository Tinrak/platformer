package platformer;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Platformer extends Game
{
	public static OrthographicCamera camera = new OrthographicCamera();
	public static final boolean DEBUGGING = true;

	private SpriteBatch normalSpriteBatch;
	private SpriteBatch hudSpriteBatch;
	private BitmapFont font;
	private MainMenu mainMenuScreen;
	private GameScreen gameScreen;

	@Override
	public void create()
	{
		System.out.println("Platformer create");
		normalSpriteBatch = new SpriteBatch();
		hudSpriteBatch = new SpriteBatch();
		font = new BitmapFont();

		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		mainMenuScreen = new MainMenu(this);
		gameScreen = new GameScreen(this);
		this.setScreen(mainMenuScreen);
	}

	@Override
	public void render()
	{
		super.render();
	}

	public void dispose()
	{
		System.out.println("Game dispose!");
		normalSpriteBatch.dispose();
		hudSpriteBatch.dispose();
		font.dispose();
	}

	public SpriteBatch getNormalSpriteBatch()
	{
		return normalSpriteBatch;
	}

	public SpriteBatch getHudSpriteBatch()
	{
		return hudSpriteBatch;
	}

	public BitmapFont getFont()
	{
		return font;
	}

	public OrthographicCamera getCamera()
	{
		return camera;
	}

	public MainMenu getMainMenu()
	{
		return mainMenuScreen;
	}

	public GameScreen getGameScreen()
	{
		return gameScreen;
	}
}
