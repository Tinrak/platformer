package platformer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;

public class GameScreen implements Screen
{
	private final Platformer game;
	private boolean pauseGame;
	private World world;
	private boolean loadGame = false;

	protected GameScreen(final Platformer game)
	{
		pauseGame = false;
		this.game = game;
	}

	@Override
	public void render(float delta)
	{
		if (!pauseGame)
		{
			if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
			{
				game.setScreen(game.getMainMenu());
				dispose();
				return;
			}
			world.update(delta);
		}
	}

	@Override
	public void resize(int width, int height)
	{
		System.out.println("Game Screen Resize");
	}

	@Override
	public void show()
	{
		System.out.println("Game Screen Show");

		if (loadGame)
		{
			System.out.println("Trying to load a game!");
		} else
		{
			world = new World();
		}
	}

	@Override
	public void hide()
	{
		System.out.println("Game Screen Hide");
		dispose();
	}

	@Override
	public void pause()
	{
		pauseGame = true;
		System.out.println("Game Screen Pause");
	}

	@Override
	public void resume()
	{
		pauseGame = false;
		System.out.println("Game Screen Resume");
	}

	@Override
	public void dispose()
	{
		System.out.println("Game Screen Dispose");
	}
}
